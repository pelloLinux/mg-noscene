#include <assert.h>
#include "line.h"
#include "tools.h"
#include "math.h"

// @@ TODO: Create a line given P (ox, oy, oz) and d (dx, dy, dz)

line *CreateLine(float ox, float oy, float oz, float dx, float dy, float dz) {

	line *thisLine;
	thisLine = malloc( sizeof(*thisLine) * 1);

	if(V_IS_ZERO(dx,dy,dz))
	{
		printf("[E] vector is zero\n");
		exit(1);
	}

	thisLine->ox = ox;
	thisLine->oy = oy;
	thisLine->oz = oz;

	thisLine->dx = dx;
	thisLine->dy = dy;
	thisLine->dz = dz;

	return thisLine;
}

// @@ TODO: Create a line which passes through two points A and B
//
// Hint: use UnitVectorPP
// Note: Check than A and B are no too close!

line *CreateLinePP(float Ax, float Ay, float Az, float Bx, float By, float Bz) {

	line * thisLine;
	thisLine = malloc( sizeof(*thisLine) * 1);

	thisLine->ox = Ax;
	thisLine->oy = Ay;
	thisLine->oz = Az;

	float mode = UnitVectorPP(&thisLine->dx, &thisLine->dy, &thisLine->dz, Ax, Ay, Az, Bx, By, Bz);

	if(mode < DISTANCE_EPSILON)
	{
		printf("[E]: A and B too close\n");
		exit(1);
	}

	return thisLine;

}

void DestroyLine(line **theLine) {
	line  *thisLine = *theLine;
	if( ! thisLine ) return;
	free( thisLine );
	*theLine = NULL;
}

// @@ TODO: Give the point corresponding to parameter t. Leave output in Px,Py,Pz

void PointInLine(line *theLine, float t, float *Px, float *Py, float *Pz) {

	*Px = theLine->ox + t*theLine->dx;
	*Py = theLine->oy + t*theLine->dy;
	*Pz = theLine->oz + t*theLine->dz;
}

// @@ TODO: Calculate the parameter 'u0' of the line point nearest to P
//
// u0 = D*(P-O) / D*D , where * == dot product

float ParamDistance(line * theLine, float Px, float Py, float Pz) {

	return ((Px-theLine->ox)*theLine->dx + (Py-theLine->oy)*theLine->dy + (Pz-theLine->oz)*theLine->dz)/((theLine->dx*theLine->dx)+(theLine->dy*theLine->dy)+(theLine->dz*theLine->dz));
}

// @@ TODO: Calculate the minimum distance 'dist' from line to P
//
// dist = mod(P - (O + uD))
// Where u = ParamDistance(theLine, float Px, float Py, float Pz)

float DistanceP(line * theLine, float Px, float Py, float Pz) {


	float x;
	float y;
	float z;

	float u = ParamDistance(theLine, Px, Py, Pz);
	x = Px-(theLine->ox + u*theLine->dx);
	y = Py-(theLine->oy + u*theLine->dy);
	z = Pz-(theLine->oz + u*theLine->dz);

	return x+y+z;
}

void PrintLine(line *theLine) {
	printf("O:(%.2f, %.2f, %.2f) d:(%.2f, %.2f, %.2f)\n",
		   theLine->ox, theLine->oy, theLine->oz,
		   theLine->dx, theLine->dy, theLine->dz);
}
