#include <GL/glew.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "tools.h"
#include "trfm3D.h"
#include "scene.h"
#include "node.h"
#include "gObjectManager.h"
#include "shaderManager.h"
#include "scenes.h"

static float step = 0.6;
static float angle_step = 1.0f * DEGREE_TO_RAD;
static float fovy = 30.0;

GObject *gobj;

static trfm3D *T;

static void InitShaders() {

	RegisterShaderScene("dummy", "Shaders/dummy.vert", "Shaders/dummy.frag");
}


void InitGL(int Width, int Height) {          // We call this right after our OpenGL window is created.


	// set OpenGL state values
	glClearColor( 0.4f, 0.4f, 0.4f, 1.0f );
	glViewport(0, 0, Width, Height);              // Reset The Current Viewport And Perspective Transformation

	// Enable culling

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Turn Depth Testing On
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f); // Also, sets GLSL fragmen shader gl_DepthRange variable

	// Aliasing

	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}


static void Display(void) {

	RenderState *rs;

	// draw the background color
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	rs = RenderStateScene();
	SetShaderProgramRS(rs, FindShaderScene("dummy"));

	LoadIdentityRS(rs, MG_MODELVIEW);
	MultTrfmRS(rs, MG_MODELVIEW, T);

	DrawGObject(gobj);
	glutSwapBuffers();
}


// Keyboard dispatcher when ALT key is pressed
static void Keyboard_alt(unsigned char key) {

	RenderState *rs;

	switch(key)
		{
		case 'v':
			rs = RenderStateScene();
			PrintTopRS(rs, MG_MODELVIEW);
			break;
		case 'p':
			rs = RenderStateScene();
			PrintTopRS(rs, MG_PROJECTION);
			break;
		case 'b':
			rs = RenderStateScene();
			SetBBoxDrawRS(rs, !GetBBoxDrawRS(rs));
			break;
		}
	glutPostRedisplay( );
}

// General keyboard dispatcher
static void Keyboard (unsigned char key, int x, int y) {

	static size_t i = 0;
	int key_mod;
	Texture *tex;

	key_mod = glutGetModifiers();
	if (key_mod == GLUT_ACTIVE_ALT) {
		// If ALT key pressed, call Keyboard_alt and exit
		Keyboard_alt(key);
		return;
	}

	switch(key)
		{
		case 's':
			// Enable Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			break;
		case 'S':
			// Disable Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case 'z':
			glEnable(GL_CULL_FACE);
			break;
		case 'Z':
			glDisable(GL_CULL_FACE);
			break;
		case 'a':
			AddRot_Y_Trfm3D(T, -angle_step);
			break;
		case 'd':
			AddRot_Y_Trfm3D(T, angle_step);
			break;
		case 'w':
			AddRot_X_Trfm3D(T, -angle_step);
			break;
		case 'x':
			AddRot_X_Trfm3D(T, angle_step);
			break;
		case 'i':
			AddTransTrfm3D(T, 0.0, 0.0, -step);
			break;
		case 'k':
			AddTransTrfm3D(T, 0.0, 0.0, step);
			break;
		case 'p':
			AddRotVecTrfm3D(T, 1.0, 0.0, 0.0, angle_step);
			break;
		case 'o':
			AddRotAxisTrfm3D(T, 1.0, 0.0, 0.0, -1, -1, 0.0, angle_step);
			break;
		case 27: // ESC
			exit(0);
			break;
		}
	glutPostRedisplay( );
}


void create_scene_tmesh() {

	float P[3];
	TriangleMesh *mesh;

	mesh = CreateVoidTriangleMesh();

	AddPointTMesh(mesh, -0.25,  0.25, -0.25); // P0
	AddPointTMesh(mesh, -0.25, -0.25, -0.25); // P1
	AddPointTMesh(mesh,  0.25, -0.25, -0.25); // P2
	AddPointTMesh(mesh,  0.25,  0.25, -0.25); // P3

	AddTriangleTMesh(mesh, 0, 1, 2); // P0 - P1 - P2
	AddTriangleTMesh(mesh, 2, 3, 0); // P2 - P3 - P0

	gobj = CreateVoidGObject();
	AddPatchGObject(gobj, mesh);
}

int main(int argc, char** argv) {

	GLenum glew_err;

	// define parameters that will be used to create windows

	// Init openGL and create a window
	srand(time(0));
	glutInit(&argc, argv);
	glutInitDisplayMode ( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowSize ( 900, 700 );
	glutInitWindowPosition ( 100, 0 );
	// create one window for OpenGL graphics
	glutCreateWindow("browser");

	// Uncomment following line if you have problems initiating GLEW
	//
	// glewExperimental = GL_TRUE;

	glew_err = glewInit();

	if (glew_err != GLEW_OK) {
		fprintf(stderr, "Error when calling glewInit: %s\n", glewGetString(glew_err));
		exit(1);
	}

	glutDisplayFunc( Display );
	glutKeyboardFunc( Keyboard );

	// Init scene
	InitScene();
	// Init openGL objects
	InitGL(900, 700);
	// Init shaders
	InitShaders();

	// Create scene
	create_scene_tmesh();

	// Transformation
	T = CreateTrfm3D();

	glutMainLoop();
	DestroyTrfm3D(&T);

	DestroyScene();

	return 0;
}

/*
 * Local Variables:
 * mode: c
 * End:
 */
