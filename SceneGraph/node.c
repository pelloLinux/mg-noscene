	#include <stdlib.h>
#include "node.h"
#include "intersect.h"
#include "bboxGL.h"
#include "scene.h"

// forward declarations of auxiliary functions

static Node* CloneParentNode(Node *sNode, Node *theParent);
static void nodeUpdateBB (Node *thisNode);
static void nodePropagateBBRoot(Node * thisNode);
static void nodeUpdateGS( Node *thisNode);

Node *CreateNode() {

	Node * thisNode;

	thisNode = malloc(sizeof(*thisNode) * 1);
	thisNode->gObject = NULL;
	thisNode->theLight = NULL;
	thisNode->theShader = NULL;
	thisNode->nodeChilds = CreateVoidList();
	thisNode->placement = CreateTrfm3D();
	thisNode->placement_WC = CreateTrfm3D();
	thisNode->container_WC = CreateVoidBBox();
	thisNode->parent = NULL;
	thisNode->isCulled = 0;
	thisNode->drawBBox = 0;

	return thisNode;
}

void DestroyNode(Node **theNode) {

	Node * oneNode;
	Node  *thisNode = *theNode;

	if( ! thisNode ) return;

	// destroy Node list
	oneNode = StartLoop(thisNode->nodeChilds);
	while(oneNode) {
		DestroyNode(&oneNode); // Recursive call
		oneNode = GetNext(thisNode->nodeChilds);
	}
	DestroyList(&thisNode->nodeChilds);

	DestroyBBox(&thisNode->container_WC);
	DestroyTrfm3D(&thisNode->placement);
	DestroyTrfm3D(&thisNode->placement_WC);
	thisNode->gObject = NULL;
	thisNode->theLight = NULL;
	thisNode->theShader = NULL;
	thisNode->parent = NULL;

	free( thisNode );

	*theNode = NULL;
}

Node * CycleChildNode(Node *theNode, size_t idx) {

	size_t   i;
	size_t   m;
	Node     *cur = NULL;

	if( theNode == NULL ) return NULL;

	m = idx % ElementsInList(theNode->nodeChilds);
	for( i = 0, cur = StartLoop(theNode->nodeChilds);
		 cur && i < m;
		 ++i, cur = GetNext(theNode->nodeChilds));
	return cur;
}

Node *NextSiblingNode(Node *theNode) {
	Node *theParent;
	Node *theSibling = NULL;

	if (!theNode) return NULL;
	theParent = theNode->parent;
	if (!theParent) return theNode;
	theSibling = StartLoop(theParent->nodeChilds);
	while(theSibling && theSibling != theNode) {
		theSibling = GetNext(theParent->nodeChilds);
	}
	theSibling = GetNext(theParent->nodeChilds);
	if (!theSibling) return StartLoop(theParent->nodeChilds);
	return theSibling;
}


Node *FirstChildNode(Node *theNode) {

	Node *theChild = NULL;

	if (!theNode) return NULL;
	theChild = StartLoop(theNode->nodeChilds);
	if (!theChild) return theNode;
	return theChild;
}

Node *ParentNode(Node *theNode) {
	if (!theNode) return NULL;
	if (!theNode->parent) return theNode;
	return theNode->parent;
}

static Node* CloneParentNode(Node *sNode, Node *theParent) {

}


Node *CloneNode(Node *sNode) {

}

// @@ TODO: auxiliary function
//
// given a node:
// - update the BBox of the node (nodeUpdateBB)
// - Propagate BBox to parent until root is reached
//
// - Preconditions:
//    - the BBox of thisNode's children are up-to-date.
//    - placement_WC of node and parents are up-to-date

static void nodePropagateBBRoot(Node * thisNode) {

}

// @@ TODO: auxiliary function
//
// given a node, update its BBox to World Coordinates so that it includes:
//  - the BBox of the geometricObject it contains (if any)
//  - the BBox-es of the children
//
// Note: Bounding box is always in world coordinates
//
// Precontitions:
//
//     * thisNode->placement_WC is up-to-date
//     * container_WC of thisNode's children are up-to-date
//
// These functions can be useful (from bbox.h):
//
// SetVoidBBox(BBox *A)
//
//    Set A to be the void (empty) BBox
//
// TransformBBox(BBox *A, BBox *B, trfm3D *T);
//
//    Transform B by applying transformation T. Leave result in A.
//
// BoxBox(BBox *A, BBox *B)
//
//    Change BBox A so it also includes BBox B
//

static void nodeUpdateBB (Node *thisNode) {

}

// @@ TODO: Update WC (world coordinates matrix) of a node and
// its bounding box recursively updating all children.
//
// given a node:
//  - update the world transformation (placement_WC) using the parent's WC transformation
//  - recursive call to update WC of the children
//  - update Bounding Box to world coordinates
//
// Precondition:
//
//  - placement_WC of thisNode->parent is up-to-date (or thisNode->parent == NULL)
//

static void nodeUpdateWC( Node *thisNode) {
	
	Node *nodoa;

	if(thisNode->parent != NULL)//gurasoari eragindako aldaketa nodoari eragin.
	{
		thisNode->placement_WC = thisNode->parent->placement_WC;
		CompositionTrfm3D(thisNode->placement_WC, thisNode->placement);//placent-en dagoen uneko aldaketa placent_WC-rekin biderkatu.
	}
	else//mundu osoari eragingo dio aldaketa(objetu guztiei)
	{
		thisNode->placement_WC = thisNode->placement;
	}

	nodoa = StartLoop(thisNode->nodeChilds);
	while(nodoa)
	{
		nodeUpdateWC(nodoa);
		nodoa = GetNext(thisNode->nodeChilds);
	}
	nodeUpdateBB(thisNode);// horaindik inplementatu gabe
}

// @@ TODO: Auxiliary function
//
// given a node:
// - Update WC of sub-tree starting at node (nodeUpdateWC)
// - Propagate Bounding Box to root (nodePropagateBBRoot)

static void nodeUpdateGS( Node *thisNode) {

}

// @@ TODO:
//
// Attach (group) a Node object into another Node
// Print a warning (and do nothing) if the node has an gObject.

void AttachNode(Node *theNode, Node *theChild) {

	if(theNode->gObject == NULL) {
		// node does not have gObject, so attach child

		theChild->parent = theNode;
		AddLast(theNode->nodeChilds, theChild);

	} else printf("theNode nodoak objektu bat du (attachNode funtzioa)\n");
	
}

void DetachNode(Node *theNode) {

	Node *theParent;

	if (theNode == NULL) return;
	theParent = theNode->parent;
	if (theParent == NULL) return; // already detached (or root node)
	theNode->parent = NULL;
	RemoveFromList(theParent->nodeChilds, theNode);
	// Update bounding box of parent
	nodePropagateBBRoot(theParent);

}

void AllChildsDetachNode(Node *theNode) {

	Node *oneNode;

	if (theNode == NULL) return;
	oneNode = StartLoop(theNode->nodeChilds);
	while(oneNode) {
		oneNode->parent = NULL;
		oneNode = GetNext(theNode->nodeChilds);
	}

	EmptyList(theNode->nodeChilds);
	nodePropagateBBRoot(theNode);

}

void SetGobjNode( Node *thisNode, GObject *gobj ) {
	if (thisNode == NULL) return;
	if (ElementsInList(thisNode->nodeChilds)) {
		fprintf(stderr, "[W] SetGobjNode: can not attach a gObject to a node with childs.\n");
		return;
	}
	thisNode->gObject = gobj;
	nodePropagateBBRoot(thisNode);
}

void SetTrfmNode( Node *thisNode, const trfm3D * M) {
	if (thisNode == NULL) return;
	SetCopyTrfm3D(thisNode->placement, M);
	// Update Geometric state
	nodeUpdateGS(thisNode);
}

void ApplyTrfmNode( Node *thisNode, const trfm3D * M) {
	if (thisNode == NULL) return;
	CompositionTrfm3D(thisNode->placement, M);
	// Update Geometric state
	nodeUpdateGS(thisNode);
}

void TransNode( Node *thisNode, float x, float y, float z ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetTransTrfm3D(&localT, x, y, z);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateXNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_X_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateYNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_Y_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void RotateZNode( Node *thisNode, float angle ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetRot_Z_Trfm3D(&localT, angle);
	ApplyTrfmNode(thisNode, &localT);
};

void ScaleNode( Node *thisNode, float factor ) {
	static trfm3D localT;

	if (thisNode == NULL) return;
	SetScaleTrfm3D(&localT, factor);
	ApplyTrfmNode(thisNode, &localT);
};

void AttachLightNode(Node *theNode, Light *theLight) {
	if (theNode == NULL) return;
	theNode->theLight = theLight;
}

void DetachLightNode(Node *theNode) {
	if (theNode == NULL) return;
	theNode->theLight = NULL;
}

void SetShaderNode(Node *theNode, ShaderProgram *theShader) {
	if (theNode == NULL) return;
	theNode->theShader = theShader;
}

void UnsetShaderNode(Node *theNode) {
	if (theNode == NULL) return;
	theNode->theShader = NULL;
}

// @@ TODO:
// Draw a (sub)tree.
//     - First spatial Objects
//     - Then children (depth-first)
//
// These functions can be useful:
//
// rs = RenderStateScene();
//
// MultTrfmRS(rs, MG_MODELVIEW, T); // Apply T transformation
// PushRS(rs, MG_MODELVIEW); // push current matrix into stack
// PopRS(rs, MG_MODELVIEW); // pop matrix from stack to current
//
// DrawGObject(gobj); // draw geometry object
//

void DrawNode(Node *theNode) {

	Node * oneNode;
	RenderState *rs;
	rs = RenderStateScene();
	
	PushRS(rs, MG_MODELVIEW);//Uneko aldaketa-matrizea pilan gorde
	MultTrfmRS(rs, MG_MODELVIEW, theNode->placement);//Transformazio lokoala eragin

	if(theNode->gObject != NULL)DrawGObject(theNode->gObject);//objetu geometrikoa marraztu
	else
	{
		oneNode = StartLoop(theNode->nodeChilds);
		if(oneNode)
			do
			{
				DrawNode(oneNode);//Dei errekurtsiboa	
			}
			while(oneNode=GetNext(theNode->nodeChilds));
	}

	PopRS(rs, MG_MODELVIEW);//matrizea pilatik atera

	// Uncomment this to toggle viewing the Bounding Box
	//
	/* if(rs->drawBBox || theNode->drawBBox)
	 		DrawGLBBox( theNode->container_WC );*/

}

// Frustum culling. See if a subtree is culled by the camera, and update
// isCulled accordingly.

void UpdateCullNode(Node *theNode,
					Camera * cam) {
}

// Check whether a BSphere (in world coordinates) intersects with a (sub)tree.
//
// Return a pointer to the Node which collides with the BSphere. NULL
// if not collision.

Node *CollisionBSphereNode(Node *thisNode, BSphere *bsph) {

	return NULL; /* No collision */
}
