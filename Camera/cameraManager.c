#include "cameraManager.h"
#include <string.h>
#include "hash.h"

static hash *cam_hash;

void InitCameraManager() {
	cam_hash = CreateVoidHash();
}


void DestroyCameraManager() {
	Camera *theCamera;
	if (cam_hash == NULL) return;
	theCamera = StartLoopHash(cam_hash);
	while(theCamera) {
		DestroyCamera(&theCamera);
		theCamera = GetNextHash(cam_hash);
	}
	DestroyHash(&cam_hash);
}


Camera *SceneFindCamera(char *cameraName) {
	return FindHashElement(cam_hash, cameraName);
}

Camera *SceneRegisterCamera(char *cameraName) {

	Camera *theCamera;
	Camera *newCamera;

	theCamera = FindHashElement(cam_hash, cameraName);
	if (!theCamera) {
		newCamera = CreateOriginCamera();
		SetCameraName(newCamera, cameraName);
		theCamera = InsertHashElement(cam_hash, cameraName, newCamera);
	} else {
		fprintf(stderr, "[W] duplicate camera %s. Skipping.\n", cameraName);
	}
	return theCamera;
}

void SceneDestroyCamera(char *cameraName) {

	Camera *theCamera;
	theCamera = FindHashElement(cam_hash, cameraName);
	if(!theCamera) return;
	DestroyCamera(&theCamera);
}

void PrintRegisteredCameras() {

	Camera *item;
	for(item = StartLoopHash(cam_hash); item; item = GetNextHash(cam_hash) ) {
		PrintCamera(item);
	}
}
