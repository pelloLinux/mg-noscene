#include "avatarManager.h"
#include <string.h>
#include "hash.h"

static hash *avatar_hash;

void InitAvatarManager() {
	avatar_hash = CreateVoidHash();
}


void DestroyAvatarManager() {
	Avatar *theAvatar;
	if (avatar_hash == NULL) return;
	theAvatar = StartLoopHash(avatar_hash);
	while(theAvatar) {
		DestroyAvatar(&theAvatar);
		theAvatar = GetNextHash(avatar_hash);
	}
	DestroyHash(&avatar_hash);
}


Avatar *SceneFindAvatar(char *avatarName) {
	return FindHashElement(avatar_hash, avatarName);
}

Avatar *SceneRegisterAvatar(char *avatarName, Camera *theCamera, int radius) {

	Avatar *theAvatar;
	Avatar *newAvatar;

	theAvatar = FindHashElement(avatar_hash, avatarName);
	if (!theAvatar) {
		newAvatar = CreateAvatar(avatarName, theCamera, radius);
		theAvatar = InsertHashElement(avatar_hash, avatarName, newAvatar);
	} else {
		fprintf(stderr, "[W] duplicate avatar %s. Skipping.\n", avatarName);
	}
	return theAvatar;
}

void SceneDestroyAvatar(char *avatarName) {

	Avatar *theAvatar;
	theAvatar = FindHashElement(avatar_hash, avatarName);
	if(!theAvatar) return;
	DestroyAvatar(&theAvatar);
}

void PrintRegisteredAvatars() {

	Avatar *item;
	for(item = StartLoopHash(avatar_hash); item; item = GetNextHash(avatar_hash) ) {
		PrintAvatar(item);
	}
}
