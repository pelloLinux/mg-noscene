#ifndef SET_H
#define SET_H

#include <stddef.h>
#include "hashlib.h"

typedef struct _sitem_st {
	char *key;
	size_t N;                // lenght of key in bytes
	struct _sitem_st *next;
} sitem_st;


typedef struct _set {
	hshtbl * htable;
	sitem_st *first_in_loop;
	sitem_st *next_in_loop;
} set;

/**
 * Create an empty set.
 *
 *
 * @return a newly created empty set.
 */

set *CreateSet();

/**
 * Destroy the set. Before destroying the set you should perhaps destroy
 * the elements the set points to (if elements are owned by the set). For
 * instance, for a set of textures:
 *
 * Texture *elem;
 *  for( item = StartLoopSet(tex_set); item; item = GetNextSet(tex_set) )
 *    DestroyTexture(&item);
 *
 * @param theSet a pointer to the set structure.
 */

void DestroySet(set **theSet);

/**
 * Insert an element into the set. The key is a string.
 *
 * @param theSet the set structure
 * @param theKey the key string
 *
 * @return 1 if inserted, 0 otherwise.
 */

int InsertSetElement(set *theSet, char *theKey);


/**
 * Find an element in the set. The key is a string.
 *
 * @param theSet the set structure
 * @param theKey the key string
 *
 * @return 0 if not present, 1 otherwise
 */

int InSetElement(set *theSet, char *theKey);


/**
 * Find an element in the set. If not there, insert a new element. The key
 * is a string.
 *
 * @param theSet the set structure
 * @param theKey the key string
 *
 * @return 1 if inserted, 0 otherwise
 */

int FindOrInsertSetElement(set *theSet, char *theKey);

/**
 * Remove a set element
 *
 * @param theSet the set structure
 * @param theKey the key string
 *
 * @return 1 if success, 0 if not.
 */

int RemoveSetElement(set *theSet, char *theKey);

/**
 * Start a loop for iterating the set. The order of elements is not specified.
 *
 * @param theSet the set structure
 *
 * @return pointer to the first element
 */
char *StartLoopSet( set *theSet );

/**
 * Get the next element of the set (order is not specified). theSet must
 * point to a valid set (initialized with StartSetLoop)
 *
 * @param theSet the set structure. It must point to a valid set (initialized with StartSetLoop)
 *
 * @return the next element in the loop.
 */

char *GetNextSet( set *theSet );

///////////////////////////////////////////////////////
//
// In the functions below the key can be anything (not just strings)

/**
 * Insert an element into the set. The key can be anything.
 *
 * @param theSet the set structure
 * @param theKey pointer to the key
 * @param N key lenght
 *
 * @return 1 if inserted, 0 otherwise.
 */

int InsertSetFreeElement(set *theSet, char *theKey, size_t N);

/**
 * Find an element in the set. The key can be anything
 *
 * @param theSet the set structure
 * @param theKey pointer to the key
 * @param N key lenght
 *
 * @return 0 if not present, 1 otherwise
 */

int InSetFreeElement(set *theSet, char *theKey, size_t N);

/**
 * Find an element in the set. If not there, insert a new element. The key
 * can be anything.
 *
 * @param theSet the set structure
 * @param theKey pointer to the key
 * @param theKey key lenght
 *
 * @return 1 if inserted, 0 otherwise
 */

int FindOrInsertSetFreeElement(set *theSet, char *theKey, size_t N);

/**
 * Remove a set element
 *
 * @param theSet the set structure
 * @param theKey pointer to the key
 * @param N key lenght
 *
 * @return 1 if success, 0 if not.
 */

int RemoveSetFreeElement(set *theSet, char *theKey, size_t N);

#endif
