
#include "JSON_parser.h"
#include "hash.h"
#include "vector.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <locale.h>

static void print_jhash(hash *h);

typedef struct _json_state {

	hash *h;
	char *last_key;
	int inside_array;
	Vector *v;

} jstate_t;

static jstate_t *create_jstate_t() {
	jstate_t * st;
	st = malloc(sizeof(*st));
	st->h = CreateVoidHash();
	st->last_key = NULL;
	st->v = NULL;
	st->inside_array = 0;
	return st;
}

// globals

static jstate_t *curState = NULL;
static int line_number = 1;

static int sc_parse(void* ctx, int type, const JSON_value* value);

static jstate_t *stack[20];
static size_t stack_n = 0;

static void init_stack() {
	stack_n = 0;
}

static void push(jstate_t *elem) {
	stack[stack_n] = elem;
	++stack_n;
}

static jstate_t *pop() {
	assert(stack_n);
	--stack_n;
	return stack[stack_n];
}

hash *parse_scene_json(char *fname) {

	int count = 0, error = 0;
	FILE* input;

	JSON_config config;

	struct JSON_parser_struct* jc = NULL;

	curState = create_jstate_t();
	curState->last_key = strdup("Scene");
	init_stack();

	init_JSON_config(&config);

	config.depth                  = 19;
	config.callback               = &sc_parse;
	config.allow_comments         = 1;
	config.handle_floats_manually = 0;

	jc = new_JSON_parser(&config);

	input = fopen(fname, "r");
	if (!input) {
		fprintf(stderr, "parse_scene: Can't open \"%s\".\n",
				fname);
		exit(1);
	}
	for (; input ; ++count) {
		int next_char = fgetc(input);
		if (next_char == '\n') line_number++;
		if (next_char <= 0) {
			break;
		}
		if (!JSON_parser_char(jc, next_char)) {
			fprintf(stderr, "JSON_parser_char: syntax error, byte %d\n", count);
			error = 1;
			goto done;
		}
	}
	if (!JSON_parser_done(jc)) {
		fprintf(stderr, "JSON_parser_end: syntax error\n");
		error = 1;
		goto done;
	}

 done:
	fclose(input);
	delete_JSON_parser(jc);
	if(!error) {
		// create objs from json_struct
		//print_json_st( json_root );
		//exit(1);
	} else {
		//delete_json_st( json_root );
		exit(1);
	}
	return curState->h;
}

static void insert_jhash(jstate_t *curState, void *v) {

	hash *h = curState->h;
	char *k = curState->last_key;

	if (!k) {
		fprintf(stderr, "[E] json_parse: invalid json, no key (line %d).\n", line_number);
		exit(1);
	}

	void *r = FindHashElement(h, k);

	if (r) {
		fprintf(stderr, "[E] json_parse: duplicate key \"%s\" (line %d).\n", k, line_number);
		exit(1);
	}
	InsertHashElement(h, k, v);
    curState->last_key = NULL;
}


static int sc_parse(void* ctx, int type, const JSON_value* value) {

	static char buf[256];
	hash *newObj;

	switch(type) {
	case JSON_T_ARRAY_BEGIN:
		curState->inside_array = 1;
		curState->v = CreateVector();
		break;
	case JSON_T_ARRAY_END:
		curState->inside_array = 0;
		insert_jhash(curState, curState->v);
		break;
	case JSON_T_OBJECT_BEGIN:
		push(curState);
		curState = create_jstate_t();
		break;
	case JSON_T_OBJECT_END:
		newObj = curState->h;
		curState = pop();
		if (curState->inside_array) {
			pushVector(curState->v, newObj);
		} else {
			insert_jhash(curState, newObj);
		}
		break;
	case JSON_T_INTEGER:
		sprintf(&buf[0], "%d", (int) value->vu.integer_value);
		if (curState->inside_array) {
			pushVector(curState->v, strdup(&buf[0]));
		} else {
			insert_jhash(curState, strdup(&buf[0]));
		}
		break;
	case JSON_T_FLOAT:
		sprintf(&buf[0], "%f", value->vu.float_value);
		if (curState->inside_array) {
			pushVector(curState->v, strdup(&buf[0]));
		} else {
			insert_jhash(curState, strdup(&buf[0]));
		}
		break;
	case JSON_T_NULL:
		printf("JSON_T_NULL\n");
		break;
	case JSON_T_TRUE:
		printf("JSON_T_TRUE\n");
		break;
	case JSON_T_FALSE:
		printf("JSON_T_FALSE\n");
		break;
	case JSON_T_KEY:
		curState->last_key = strdup(value->vu.str.value);
		break;
	case JSON_T_STRING:
		insert_jhash(curState, strdup(value->vu.str.value));
		break;
	default:
		assert(0);
		break;
	}
	return 1;
}


/* int main(int argc, char *argv[]) { */

/* 	//char *fname = "scene_json.txt"; */
/* 	char *fname = "kk.json"; */
/* 	hash *h; */
/* 	if (argc > 1) fname = argv[1]; */
/* 	h = parse_scene(fname); */
/* 	//print_jhash(h); */
/* 	return 0; */
/* } */
